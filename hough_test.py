# -*- coding: utf-8 -*-
"""
Created on Fri Aug 27 13:23:13 2021

@author: Oliver.Matthews
"""
import numpy as np
import matplotlib.pyplot as plt

def vote(vote_table, i, j):
    vote_table[i,j] += 1
    i_min = max([i - 4, 0])
    i_max = min([i + 4, vote_table.shape[0] - 1])
    j_min = max([j - 4, 0])
    j_max = min([j + 4, vote_table.shape[1] - 1])
    vote_table[i_min: i_max, j_min:j_max] += 1
    return vote_table

def hough(a_grid, k_grid, vote_table, xs, ys):
    for x, y in zip(xs, ys):
        for i, k in enumerate(k_grid):
            j = np.digitize(y * np.exp(k * x), a_grid)
            if j < len(a_grid) and j > 0:
                vote_table = vote(vote_table, i, j)
                
    k_index, a_index = np.where(vote_table == np.amax(vote_table))
    k_index = k_index[0]
    a_index = a_index[0]
    certainty = np.amax(vote_table) / np.sum(vote_table)
    if k_index <= 5 or k_index >= len(k_grid) - 5 or a_index <= 5 or a_index >= len(a_grid) - 5:
        print('Warning: You are in the edge of the search region')
        print(a_index, k_index, print(certainty))

    k_voted = np.mean([k_grid[k_index], k_grid[k_index - 1]])
    a_voted = np.mean([a_grid[a_index], a_grid[a_index - 1]])
    
    return a_voted, k_voted, certainty, vote_table

if __name__ == '__main__':

    # Set up data
    x_array = np.arange(0, 2000)
    
    a = 2
    k = 0.001
    
    
    ys = a * np.exp(x_array * - k)
    
    noisy_ys = ys + np.random.normal(scale = 0.1, size = ys.shape)
    
    plt.plot(x_array, noisy_ys)
    plt.show()
    
    
    # Set up search space
    a_min = 0
    a_max = 4
    n_a = 100
    
    k_min = 0
    k_max = 0.002
    n_k = 100
     
    a_grid = np.linspace(a_min, a_max, n_a)
    k_grid = np.linspace(k_min, k_max, n_k)
    vote_table = np.zeros((n_a, n_k))
    
    a_voted, k_voted, vote_table = hough(a_grid, k_grid, vote_table, x_array, noisy_ys)
    
    plt.imshow(vote_table)
    
    print(k_voted, a_voted)