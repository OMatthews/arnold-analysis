# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 17:03:47 2021

@author: Oliver.Matthews
"""
import os
import matplotlib.pyplot as plt
from pathlib import Path
from PIL import Image
import numpy as np
import matplotlib.patches as patches
import openpyxl
import av
import pickle
import argparse
import pandas as pd
from scipy import signal
from hough_test import hough

from IPython import get_ipython

width = 20
height = 20
left_wall_offset = 20

base_folder_path = Path(r'D:\\')
save_path = Path.home() / 'arnold-intensity-plotter'
bbox_folder = save_path / 'bboxes'
image_folder = save_path / 'images'
roi_sc_folder = save_path / 'roi_screenshots'
out_csv_file = save_path / 'results.csv'
filtered_csv_file = save_path / 'filtered_results.csv'
steps_csv_file = save_path / 'step_sizes.csv'

test_plan_csv = Path(r'C:\Users\Oliver.Matthews\TTPGroup\Arnold 2 - Documents\General\05-Design Outputs\Rig Data') / 'Arnold Validation Rig Test Plan.xlsx'
test_plan_df = pd.read_excel(test_plan_csv, header = 3, sheet_name = 'Sheet1')

save_frame = 600

filter_b, filter_a = signal.iirfilter(9, 0.01, btype = 'lowpass', fs = 1.0)
filter_zi = signal.lfilter_zi(filter_b, filter_a)

max_min_window = 500


# Vote table for hough transform
a_min = 0
a_max = 30
n_a = 100

k_min = 0
k_max = 0.005
n_k = 100
 
a_grid = np.linspace(a_min, a_max, n_a)
k_grid = np.linspace(k_min, k_max, n_k)
vote_table = np.zeros((n_k, n_a))



def update_csv(csv_file, filename, values):
    if not os.path.exists(csv_file):
        with open(csv_file, 'w') as file:
            file.write('Frame Number')
    df = pd.read_csv(csv_file, index_col = 0)
    if len(values) > len(df):
        df = df.reindex(list(range(len(values))))
    else:
        values += [np.nan] * (len(df) - len(values))
    df[filename] = values
    df.to_csv(csv_file)
    
    
    
def check_folder(name):
    try:
        int(name[1:])
        return True
    except:
        return False

def set_bbox(letter):
    print(letter)
    get_ipython().run_line_magic('matplotlib', 'qt')

    # num = input(f'Which number video do you want to use to set the bbox for letter {letter}')
    num = str(24)
    folder_path = base_folder_path / (letter + num)
    video = [folder_path / f for f in os.listdir(folder_path) if f.endswith('.avi')][0]
    
    with av.open(str(folder_path / video)) as container:
        # The first stage is to get the user to pick out the ROI
        for i, frame in enumerate(container.decode()):
            # We only look at every 100 frames for this part
            if not (i % 100) == 0:
                continue
            im = frame.to_image()
            fig, ax = plt.subplots()
            plt.axis('off')
            ax.imshow(im)
            plt.title(f'Click which region to survey. Middle click to skip\nalong the video by 100. Frame {i}')
            click_coords = plt.ginput(1)
            if len(click_coords) == 0:
                plt.close()
                continue
            else:
                roi_coords = click_coords[0]
            rect = patches.Rectangle((roi_coords[0] - width / 2, roi_coords[1] - height / 2), width, height, linewidth=1, edgecolor='r', facecolor='none')
            ax.add_patch(rect)
            
            plt.title('Click where you want the horizontally aligned background to be\n(N.B. we will ignore the vertical position and set\nit in line with the ROI)')
            fig.canvas.draw_idle()  
            ref_hor_coords = (plt.ginput(1)[0][0], roi_coords[1]) 

            plt.title('Click where you want the vertically aligned background to be\n(N.B. we will ignore the horizontal position and set\nit in line with the ROI)')
            fig.canvas.draw_idle()  
            ref_vert_coords = (roi_coords[0], plt.ginput(1)[0][1]) 
            ref_diag_coords = (ref_hor_coords[0], ref_vert_coords[1]) 
            plt.close()
            
            rois = {'roi' : {'coords' : roi_coords},
                    'ref_hor' : {'coords' : ref_hor_coords},
                    'ref_vert' : {'coords' : ref_vert_coords},
                    'ref_diag' : {'coords' : ref_diag_coords}}
            
            for key, roi in rois.items():
                rois[key]['i_start'] = int(round(roi['coords'][1] - height // 2))
                rois[key]['i_end'] = int(round(roi['coords'][1] + height // 2))
                rois[key]['j_start'] = int(round(roi['coords'][0] - width // 2))
                rois[key]['j_end'] = int(round(roi['coords'][0] + width // 2))
            break
        
     
    if not os.path.exists(bbox_folder):
        os.mkdir(bbox_folder)
    with open(bbox_folder / (letter + '.p'), 'wb') as savefile:
        pickle.dump(rois, savefile)

def plot_intensities():
    get_ipython().run_line_magic('matplotlib', 'inline')
    all_worked = True
    if not os.path.exists(image_folder):
        os.mkdir(image_folder)
    if not os.path.exists(roi_sc_folder):
        os.mkdir(roi_sc_folder)
    if not os.path.exists(steps_csv_file):
        with open(steps_csv_file, 'w') as fd:
            fd.write('Experiment,Step Change')
            
    step_change_df = pd.read_csv(steps_csv_file)
            
    warned = set()
    for subdir in os.listdir(base_folder_path):
        if not check_folder(subdir):
            continue
        im_savepath = image_folder / (subdir + '.png')
        if os.path.exists(im_savepath):
            continue
        # try:
        print(subdir)
        letter = subdir[0]
        if not os.path.exists(bbox_folder) or not letter + '.p' in os.listdir(bbox_folder):
            if not letter in warned:
                warned.add(letter)
                print(f'No bbox found for letter prefix {letter}. Skipping files with this prefix.')
                all_worked = False
            continue
        else:
            with open(bbox_folder / (letter + '.p'), 'rb') as savefile:
                rois = pickle.load(savefile)

        folder_path = base_folder_path / subdir
        videos = [folder_path / f for f in os.listdir(folder_path) if f.endswith('.avi')]
        
        if len(videos) > 1:
            print(f'More than one video found in folder {folder_path}. Skipping')
            continue
                
        for video in videos:
            frame_ns = []
            roi_means = []
            ref_hor_means = []
            ref_vert_means = []
            for key in rois.keys():
                rois[key]['means'] = []
            with av.open(str(folder_path / video)) as container:
                for i, frame in enumerate(container.decode()):
                    if i == save_frame:
                        im = frame.to_image()
                        fig, ax = plt.subplots()
                        plt.axis('off')
                        ax.imshow(im)
                        rect = patches.Rectangle((rois['roi']['coords'][0] - width / 2, rois['roi']['coords'][1] - height / 2), width, height, linewidth=1, edgecolor='r', facecolor='none')
                        ax.add_patch(rect)
                        rect2 = patches.Rectangle((rois['ref_hor']['coords'][0] - width / 2, rois['ref_hor']['coords'][1] - height / 2), width, height, linewidth=1, edgecolor='b', facecolor='none')
                        ax.add_patch(rect2)
                        rect3 = patches.Rectangle((rois['ref_vert']['coords'][0] - width / 2, rois['ref_vert']['coords'][1] - height / 2), width, height, linewidth=1, edgecolor='b', facecolor='none')
                        ax.add_patch(rect3)
                        plt.title('Chosen regions. ROI in red, REF in blue.')
                        plt.savefig(roi_sc_folder / (subdir + '.png'))
                        plt.clf()
                    im_array = np.array(frame.to_image())

                    frame_ns.append(i)
                    for key, val in rois.items():
                        rois[key]['means'].append(np.mean(im_array[val['i_start'] : val['i_end'], val['j_start'] : val['j_end']]))
                   
            for key, val in rois.items():

                rois[key]['filtered'], _ = signal.lfilter(filter_b, filter_a, val['means'], zi = filter_zi * val['means'][0])
                # plt.plot(rois[key]['filtered'])
                # plt.show()
            global fs
            fs = rois['roi']['filtered'] + rois['ref_diag']['filtered'] - rois['ref_hor']['filtered'] - rois['ref_vert']['filtered']            
            
            plt.plot(fs)
            plt.title(subdir)
            plt.xlabel('Frame number')
            plt.ylabel('Mean intensity')
            
            # plt.savefig(im_savepath)
            # plt.clf()
            # update_csv(out_csv_file, subdir, fs)
            # # update_csv(filtered_csv_file, subdir, list(roi_corrected))
            
            a_voted, k_voted, certainty, vt = hough(a_grid, k_grid, vote_table.copy(), np.arange(len(fs)), fs)
            
            plt.plot(np.arange(len(fs)), a_voted * np.exp(- k_voted * np.arange(len(fs))))
            plt.show()
            plt.imshow(vt)
            plt.show()
            max_filtered_arg = np.argmax(fs)
            if max_filtered_arg < max_min_window:
                step_change = 0
            else:
                min_filtered = np.min(fs[max_filtered_arg - max_min_window : max_filtered_arg])
            step_change = fs[max_filtered_arg] - min_filtered
            step_change_df = step_change_df.append(pd.DataFrame([[subdir, step_change, a_voted, k_voted, certainty, test_plan_df.loc[test_plan_df['Individual Identifier'] == subdir]['Leak flow rate'].to_list()[0]]], columns = ['Experiment', 'Step Change', 'Exp Multiplier', 'Exp Exponent', 'Certainty', 'Leak Flow Rate (m_3/h)']))
            step_change_df.to_csv(steps_csv_file, index = False)
            
            plt.close()
                
        # except Exception as e:
        #     print(f'Error on file {subdir}')
        #     print(e)
        #     continue
            
            
    return all_worked

if __name__ == '__main__':
    # parser = argparse.ArgumentParser()
    # parser.add_argument('--setbbox', help='Set this to set the bboxes. Must be done before the script can be run.', action="store_true")
    # parser.add_argument('bboxprefix', nargs="?", default = 'all', help='The letter to set the bbox for. Ignored if --set-bbox is not defined. Set to "all" to do all prefixes.')
    
    # args = parser.parse_args()

    # if args.setbbox:
    #     if args.bboxprefix == 'all':
    #         done_letters = set()
    #         for file in os.listdir(base_folder_path):
    #             prefix = file[0]
    #             if not prefix in done_letters:
    #                 done_letters.add(prefix)
    #                 set_bbox(prefix)
    #     else:             
    #         set_bbox(args.bboxprefix)
    if not plot_intensities():
        done_letters = set()
        for file in os.listdir(base_folder_path):
            if not check_folder(file):
                continue
            prefix = file[0]
            if not prefix in done_letters:
                done_letters.add(prefix)
                print(file)
                set_bbox(prefix)
        plot_intensities()
